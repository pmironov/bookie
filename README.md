# README #

This is a test project for a book list. The app allows to view the list of books and add/edit/remove the book to that list.

# Technologies#

* .NET 4.5

* ASP.NET MVC 4

* AngularJS 1.5.8

* Entity Framework 6

# Tests #

Tests are covering the validation for book creating function.