﻿var App = angular.module('App', ['ngRoute']);

App.controller('LandingPageController', LandingPageController);
App.controller('HomeController', HomeController);
App.controller('BookInfoController', BookInfoController);
App.controller('AddBookController', AddBookController);

App.factory('HomeFactory', HomeFactory);

// directive for singe book item view in books list
App.directive('book', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'book.html'
    }
});
// directive for singe author item view in authors list
App.directive('author', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'author.html'
    }
});

//directive to check the title to be less than 31 char
App.directive('titleValid', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue.length <= 30) {
                    modelCtrl.$setValidity('titleValid', true);
                    return inputValue;
                } else {
                    modelCtrl.$setValidity('titleValid', false);
                    return modelCtrl.$modelValue;
                }
            });
        }
    };
});

//directive to check pages count agains the specified pattern (0-10000)
App.directive('pagesCountValid', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (parseInt(inputValue, 10) <= 10000 && parseInt(inputValue, 10) > 0) {
                    modelCtrl.$setValidity('pagesCountValid', true);
                    return inputValue;
                } else {
                    modelCtrl.$setValidity('pagesCountValid', false);
                    return modelCtrl.$modelValue;
                }
            });
        }
    };
});

//directive to check publication year to be greater than 1800
App.directive('publicationYearValid', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (parseInt(inputValue, 10) >= 1800) {
                    modelCtrl.$setValidity('publicationYearValid', true);
                    return inputValue;
                } else {
                    modelCtrl.$setValidity('publicationYearValid', false);
                    return modelCtrl.$modelValue;
                }
            });
        }
    };
});

//directive to check publisher name to be less than 31 character
App.directive('publisherValid', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue.length <= 30) {
                    modelCtrl.$setValidity('publisherValid', true);
                    return inputValue;
                } else {
                    modelCtrl.$setValidity('publisherValid', false);
                    return modelCtrl.$modelValue;
                }
            });
        }
    };
});

//directive to check for valid ISBN (13-digit value) number
App.directive('isbnValid', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                var isbnPattern = /^(?=.{17}$)97(?:8|9)([ -])\d{1,5}\1\d{1,7}\1\d{1,6}\1\d$/;
                if (isbnPattern.test(inputValue)) {
                    modelCtrl.$setValidity('isbnValid', true);
                    return inputValue;
                } else {
                    modelCtrl.$setValidity('isbnValid', false);
                    return modelCtrl.$modelValue;
                }
            });
        }
    };
});

// service to store and get shared object between controllers
App.service('sharedProperties', function () {
    var books = [];
    var currentBook = {};

    return {
        getBooks: function () {
            return books;
        },
        getCurrentBook: function () {
            return currentBook;
        },
        setBooks: function (value) {
            books = value;
        },
        setCurrentBook: function (value) {
            currentBook = value;
        }
    }
});

var configFunction = function ($routeProvider, $httpProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'default.html',
            controller: HomeController
        })
        .when('/bookInfo/:bookIndex', {
            templateUrl: 'bookInfo.html',
            controller: BookInfoController
        })
        .when('/add', {
            templateUrl: 'addBook.html',
            controller: AddBookController
        });
}
configFunction.$inject = ['$routeProvider', '$httpProvider'];

App.config(configFunction);