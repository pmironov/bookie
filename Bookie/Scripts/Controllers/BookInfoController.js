﻿var BookInfoController = function ($scope, $routeParams, HomeFactory, sharedProperties) {
    var index = $routeParams.bookIndex;
    sharedProperties.setCurrentBook(sharedProperties.getBooks()[index]);
    $scope.currentBook = sharedProperties.getCurrentBook();

    $scope.currentAuthor = {};
    // call HomeFactory to edit the book
    $scope.editBook = function () {
        if (location.href.indexOf('bookInfo') < 0) {
            return;
        }
        $scope.currentBook = sharedProperties.getCurrentBook();
        $scope.books = sharedProperties.getBooks();
        var result = HomeFactory().EditBook($scope.currentBook);
        result.then(function (result) {
            if (result.success) {
                alert('The book edited successfully');
            } else {
                alert('Edit failed, please try again later.');
            }
        });
    }

    // call HomeFactory to remove Book
    $scope.removeBook = function () {
        if (location.href.indexOf('bookInfo') < 0) {
            return;
        }
        if (confirm('Are you sure you want to remove this book?')) {
            $scope.currentBook = sharedProperties.getCurrentBook();
            $scope.books = sharedProperties.getBooks();
            var bookId = $scope.currentBook.ID;
            var result = HomeFactory().RemoveBook(bookId);
            result.then(function (result) {
                if (result.success) {
                    var index = $scope.books.indexOf($scope.currentBook);
                    $scope.books.splice(index, 1);
                    sharedProperties.setBooks($scope.books);
                    alert('The book removed successfully');
                } else {
                    alert('Error removing the book. Please try again later.');
                }
            });
        }
    }

    // prompt user to change book image url
    $scope.changeImage = function () {
        var url = prompt("Please enter image url", "");
        if (url != null) {
            $scope.currentBook.ImageURL = url;
        }
    }

    // add author to the book
    $scope.addAuthor = function () {
        if (location.href.indexOf('bookInfo') < 0) {
            return;
        }
        var newAuthor = {};
        newAuthor.LastName = $scope.currentAuthor.LastName;
        newAuthor.FirstName = $scope.currentAuthor.FirstName;
        $scope.currentBook = sharedProperties.getCurrentBook();
        $scope.currentBook.Authors.push(newAuthor);
        sharedProperties.setCurrentBook($scope.currentBook);        
    }
}

BookInfoController.$inject = ['$scope', '$routeParams', 'HomeFactory', 'sharedProperties'];
