﻿var HomeController = function ($scope, $routeParams, $location, HomeFactory, sharedProperties) {
    $scope.models = {
        titleText: 'Bookee'        
    };
    
    // set order field
    $scope.setOrder = function (orderField) {
        if (orderField) {
            $scope.order = orderField;
            localStorage.setItem('order', $scope.order);
        }
    };

    //set order direction
    $scope.setDirection = function () {
        $scope.direction = !$scope.direction;
        localStorage.setItem('direction', $scope.direction);
        
    };

    // call HomeFactory to return list of books
    $scope.init = function () {
        var result = HomeFactory().LoadBooks();
        result.then(function (result) {
            if (result.success) {
                var data = result.data;
                sharedProperties.setBooks(data);
                $scope.books = data;
            } else {
                alert('Failed to load the data! Something is very wrong :(');
            }
        });
        // in order to decide whether we want to use saved variables, here we can add check for some time expired since last load OR we can handle $locationChangeStart to clear localstorage on app close
        var savedOrder = localStorage.getItem('order');
        var savedDirection = localStorage.getItem('direction');

        $scope.order = savedOrder;
        $scope.direction = savedDirection;
    };
}

HomeController.$inject = ['$scope', '$routeParams', '$location', 'HomeFactory', 'sharedProperties'];
