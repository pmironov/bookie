﻿var AddBookController = function ($scope, $location, HomeFactory, sharedProperties) {
    sharedProperties.setCurrentBook(null);
    $scope.currentBook = []; // fields change after user input

    $scope.currentAuthor = {};
    // call HomeFactory to add book
    $scope.addBook = function () {
        if (!$scope.currentBook.Authors ) {
            alert('Please add at least one author');
            return;
        }
        var result = HomeFactory().AddBook($scope.currentBook);
        result.then(function (result) {
            if (result.success) {
                $scope.books = sharedProperties.getBooks();
                var res = result.data;
                var book = $scope.currentBook;
                book.ID = res;
                $scope.books.push(book);
                sharedProperties.setBooks($scope.books);
            }
            else {
                alert('Failed to create the Book');
            }
        });
        sharedProperties.setCurrentBook(null);
    };

    // prompt user to change book image url
    $scope.changeImage = function () {
        var url = prompt("Please enter image url", "");
        if (url != null) {
            $scope.currentBook.ImageURL = url;
        }
    }

    $scope.addAuthor = function () {
        if (location.href.indexOf('add') < 0) {
            return;
        }
        var newAuthor = {};
        newAuthor.LastName = $scope.currentAuthor.LastName;
        newAuthor.FirstName = $scope.currentAuthor.FirstName;
        $scope.currentBook.Authors = [];
        $scope.currentBook.Authors.push(newAuthor);
        sharedProperties.setCurrentBook($scope.currentBook);
    }
}


AddBookController.$inject = ['$scope', '$location', 'HomeFactory', 'sharedProperties'];
