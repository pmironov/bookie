﻿var HomeFactory = function ($http, $q) {

    // load list of Books
    function loadBooks() {
        var deferredObject = $q.defer();
        $http.post(
            '/Home/LoadBooks'
        ).
        success(function (data) {
            deferredObject.resolve({ success: true, data: data });
        }).
        error(function () {
            deferredObject.resolve({ success: false });
        });

        return deferredObject.promise;
    };

    // add Book - sending new Book object, ID will be returned as result
    function addBook(book) {
        var deferredObject = $q.defer();
        $http.post(
            '/Home/AddBook',
            {
                Title: book.Title,
                Authors: book.Authors,
                LastName: book.LastName,
                PageCount: book.PageCount,
                Publisher: book.Publisher,
                PublicationYear: book.PublicationYear,
                ISBN: book.ISBN,
                ImageURL: book.ImageURL
            }
        ).
        success(function (data) {
            deferredObject.resolve({ success: true, data: data });
        }).
        error(function () {
            deferredObject.resolve({ success: false });
        });

        return deferredObject.promise;
    };

    // edit Book - sending changed Book with old ID and new fields, result is true/false
    function editBook(book) {
        var deferredObject = $q.defer();
        $http.post(
            '/Home/EditBook',
            {
                ID: book.ID,
                Title: book.Title,
                Authors: book.Authors,
                LastName: book.LastName,
                PageCount: book.PageCount,
                Publisher: book.Publisher,
                PublicationYear: book.PublicationYear,
                ISBN: book.ISBN,
                ImageURL: book.ImageURL
            }
        ).
        success(function (data) {
            deferredObject.resolve({ success: true, data: data });
        }).
        error(function () {
            deferredObject.resolve({ success: false });
        });

        return deferredObject.promise;
    };

    // remove Book - sending ID of the book to remove, result is true/false
    function removeBook(bookId) {
        var deferredObject = $q.defer();
        $http.post(
            '/Home/RemoveBook',
            {
                BookID: bookId
            }
        ).
        success(function (data) {
            deferredObject.resolve({ success: true, data: data });
        }).
        error(function () {
            deferredObject.resolve({ success: false });
        });

        return deferredObject.promise;
    }
    return function () {
        return {
            LoadBooks: function () {
                return loadBooks();
            },
            AddBook: function (book) {
                return addBook(book);
            },
            EditBook: function (book) {
                return editBook(book);
            },
            RemoveBook: function (id) {
                return removeBook(id);
            }
        }
    };
};


HomeFactory.$inject = ['$http', '$q'];