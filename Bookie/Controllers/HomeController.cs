﻿using Bookee.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Bookee.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public string LoadBooks(LoadBooksModel model)
        {
            model.LoadBooks();            
            var res = new JavaScriptSerializer().Serialize(model.Books);
            return res;
        }

        [HttpPost]
        [AllowAnonymous]
        public string AddBook(AddBookModel model)
        {
            model.AddBook();
            var res = new JavaScriptSerializer().Serialize(model.NewBookID);
            return res;
        }

        [HttpPost]
        [AllowAnonymous]
        public string EditBook(EditBookModel model)
        {
            model.EditBook();
            var res = new JavaScriptSerializer().Serialize(model.Success);
            return res;
        }

        [HttpPost]
        [AllowAnonymous]
        public string RemoveBook(RemoveBookModel model)
        {
            model.RemoveBook();
            var res = new JavaScriptSerializer().Serialize(model.Success);
            return res;
        }

    }
}
