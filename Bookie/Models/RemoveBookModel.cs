﻿using Bookee.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookee.Models
{
    public class RemoveBookModel
    {
        /// <summary>
        /// ID of the book to remove
        /// </summary>
        public int BookID { get; set; }

        /// <summary>
        /// Returned back to the UI as the result of the operation
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Call service to remove the book
        /// </summary>
        public void RemoveBook()
        {
            var svc = new OperationService();
            svc.RemoveBook(BookID);

            this.Success = svc.Result;
        }
    }
}