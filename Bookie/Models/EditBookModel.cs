﻿using Bookee.Core.DataModel;
using Bookee.Core.Interfaces;
using Bookee.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookee.Models
{
    public class EditBookModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public List<Author> Authors { get; set; }
        public int PageCount { get; set; }
        public string Publisher { get; set; }
        public int PublicationYear { get; set; }
        public string ISBN { get; set; }
        public string ImageURL { get; set; }

        /// <summary>
        /// Returned back to the UI as the result of the operation
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Call service to edit the book
        /// </summary>
        public void EditBook()
        {
            Book editedBook = new Book()
            {
                ID = this.ID,
                Title = this.Title,
                Authors = this.Authors,
                PageCount = this.PageCount,
                Publisher = this.Publisher,
                PublicationYear = this.PublicationYear,
                ISBN = this.ISBN,
                ImageURL = this.ImageURL
            };
            var svc = new OperationService();
            svc.EditBook(editedBook);

            this.Success = svc.Result;
        }
    }
}