﻿using Bookee.Core.DataModel;
using Bookee.Core.Interfaces;
using Bookee.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookee.Models
{
    public class LoadBooksModel
    {
        /// <summary>
        /// Returned back to the UI as the source of the books list
        /// </summary>
        public List<IBook> Books = new List<IBook>();

        /// <summary>
        /// Call service to load books
        /// </summary>
        public void LoadBooks()
        {
            var svc = new OperationService();
            svc.LoadBooks();

            this.Books = svc.Result;
        }        
    }
}