﻿using Bookee.Core.DataModel;
using Bookee.Core.Interfaces;
using Bookee.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookee.Models
{
    public class AddBookModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public List<Author> Authors { get; set; }
        public int PageCount { get; set; }
        public string Publisher { get; set; }
        public int PublicationYear { get; set; }
        public string ISBN { get; set; }
        public string ImageURL { get; set; }

        /// <summary>
        /// Returned back to the UI to insert to the books list with correct ID
        /// </summary>
        public int NewBookID { get; private set; }

        /// <summary>
        /// Call service to add new book
        /// </summary>
        public void AddBook()
        {
            var book = new Book()
            {
                Title = this.Title,
                Authors = this.Authors,
                PageCount = this.PageCount,
                Publisher = this.Publisher,
                PublicationYear = this.PublicationYear,
                ISBN = this.ISBN,
                ImageURL = this.ImageURL
            };
            var svc = new OperationService();
            svc.AddBook(book);

            book.ID = svc.Result;
            this.NewBookID = book.ID;
        }
    }
}