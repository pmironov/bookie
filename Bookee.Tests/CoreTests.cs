﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bookee.Core.Services;
using Bookee.Core.DataModel;
using System.Collections.Generic;
using Bookee.Core.Classes;

namespace Bookee.Tests
{
    [TestClass]
    public class CoreTests
    {
        [TestMethod]
        public void LoadBooks_ShouldReturnSuccess()
        {
            var service = new OperationService();
            service.LoadBooks();
            Assert.AreEqual((int)ResponseCodes.Success, service.Code);
        }

        [TestMethod]
        public void AddBook_WithAllFields_ShouldReturnSuccess()
        {
            var service = new OperationService();
            var book = new Book()
            {
                Title = "My own book " + new Random().Next(0, 1000),
                Authors = new List<Author>() { new Author() { FirstName = "Pavel", LastName = "Mironov" } },
                ISBN = "978-1-56619-909-4",
                ImageURL = "http://shop.startrek.com/imgcache/product/resized/000/972/151/catl/star-trek-friendship-necklace-545_1000.jpg?k=17adf910&pid=972151&s=catl&sn=startrek",
                PageCount = new Random().Next(1, 10000),
                PublicationYear = new Random().Next(1800, 2016),
                Publisher = "Publishing House of Pavel"
            };
            service.AddBook(book);
            Assert.AreEqual((int)ResponseCodes.Success, service.Code);
        }

        [TestMethod]
        public void AddBook_WithLongPublisherName_ShouldReturnError()
        {
            var service = new OperationService();
            var book = new Book()
            {
                Title = "My own book",
                Authors = new List<Author>() { new Author { FirstName = "Pavel", LastName = "Mironov" } },
                ISBN = "978-1-56619-909-4",
                ImageURL = "http://shop.startrek.com/imgcache/product/resized/000/972/151/catl/star-trek-friendship-necklace-545_1000.jpg?k=17adf910&pid=972151&s=catl&sn=startrek",
                PageCount = 1000,
                PublicationYear = 2016,
                Publisher = "Publishing House of Pavel Mironov"
            };
            service.AddBook(book);
            Assert.AreEqual((int)ResponseCodes.FailedToAddBook, service.Code);
        }

        [TestMethod]
        public void AddBook_WithNoTitle_ShouldReturnError()
        {
            var service = new OperationService();
            var book = new Book()
            {
                Title = string.Empty,
                Authors = new List<Author>() { new Author { FirstName = "Pavel", LastName = "Mironov" } },
                ISBN = "978-1-56619-909-4",
                ImageURL = "http://shop.startrek.com/imgcache/product/resized/000/972/151/catl/star-trek-friendship-necklace-545_1000.jpg?k=17adf910&pid=972151&s=catl&sn=startrek",
                PageCount = 1000,
                PublicationYear = 2016,
                Publisher = "Publishing House of Pavel Mironov"
            };
            service.AddBook(book);
            Assert.AreEqual((int)ResponseCodes.FailedToAddBook, service.Code);
        }

        [TestMethod]
        public void AddBook_WithLongTitle_ShouldReturnError()
        {
            var service = new OperationService();
            var book = new Book()
            {
                Title = "My own book is so good that the title of it should be at least 76 characters",
                Authors = new List<Author>() { new Author { FirstName = "Pavel", LastName = "Mironov" } },
                ISBN = "978-1-56619-909-4",
                ImageURL = "http://shop.startrek.com/imgcache/product/resized/000/972/151/catl/star-trek-friendship-necklace-545_1000.jpg?k=17adf910&pid=972151&s=catl&sn=startrek",
                PageCount = 1000,
                PublicationYear = 2016,
                Publisher = "Publishing House of Pavel"
            };
            service.AddBook(book);
            Assert.AreEqual((int)ResponseCodes.FailedToAddBook, service.Code);
        }

        [TestMethod]
        public void AddBook_WithBadISBN_ShouldReturnError()
        {
            var service = new OperationService();
            var book = new Book()
            {
                Title = "My own book",
                Authors = new List<Author>() { new Author { FirstName = "Pavel", LastName = "Mironov" } },
                ISBN = "WHAT IS THE ISBN?",
                ImageURL = "http://shop.startrek.com/imgcache/product/resized/000/972/151/catl/star-trek-friendship-necklace-545_1000.jpg?k=17adf910&pid=972151&s=catl&sn=startrek",
                PageCount = 1000,
                PublicationYear = 2016,
                Publisher = "Publishing House of Pavel"
            };
            service.AddBook(book);
            Assert.AreEqual((int)ResponseCodes.FailedToAddBook, service.Code);
        }

        [TestMethod]
        public void AddBook_WithNoAuthors_ShouldReturnError()
        {
            var service = new OperationService();
            var book = new Book()
            {
                Title = "My own book",
                Authors = new List<Author>() {  },
                ISBN = "978-1-56619-909-4",
                ImageURL = "http://shop.startrek.com/imgcache/product/resized/000/972/151/catl/star-trek-friendship-necklace-545_1000.jpg?k=17adf910&pid=972151&s=catl&sn=startrek",
                PageCount = 1000,
                PublicationYear = 2016,
                Publisher = "Publishing House of Pavel"
            };
            service.AddBook(book);
            Assert.AreEqual((int)ResponseCodes.FailedToAddBook, service.Code);
        }

        [TestMethod]
        public void AddBook_WithNoPages_ShouldReturnError()
        {
            var service = new OperationService();
            var book = new Book()
            {
                Title = "My own book",
                Authors = new List<Author>() { },
                ISBN = "978-1-56619-909-4",
                ImageURL = "http://shop.startrek.com/imgcache/product/resized/000/972/151/catl/star-trek-friendship-necklace-545_1000.jpg?k=17adf910&pid=972151&s=catl&sn=startrek",
                PublicationYear = 2016,
                Publisher = "Publishing House of Pavel"
            };
            service.AddBook(book);
            Assert.AreEqual((int)ResponseCodes.FailedToAddBook, service.Code);
        }
    }
}
