﻿using Bookee.Core.DataModel;
using System.Data.Entity;

namespace Bookee.Core.Context
{
    public class BooksContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
    }
}
