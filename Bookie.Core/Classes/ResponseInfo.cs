﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookee.Core.Classes
{
    public enum ResponseCodes
    {
        Success = 1,
        InternalError = -1,
        FailedToLoadBooks = -100,
        FailedToAddBook = -200,
        FailedToEditBook = -300,
        FailedToRemoveBook = -400,
    }

    public class ResponseMessages
    {
        public static string Success = "Success.";
        public static string InternalError = "Unable to perform the operation. Internal error occured. Please try again later.";
        public static string FailedToLoadBooks = "Unable to load books list. Please try again later.";
        public static string FailedToAddBook = "Unable to add the book. Please try again later.";
        public static string FailedToEditBook = "Unable to edit the book. Please try again later.";
        public static string FailedToRemoveBook = "Unable to remove the book. Please try again later.";
    }
}
