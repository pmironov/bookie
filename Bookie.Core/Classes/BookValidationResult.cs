﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookee.Core.Classes
{
    public class BookValidationResult
    {
        public static string OK = "OK";
        public static string IsTooLong = "is too long";
        public static string IsEmpty = "is empty";
        public static string ISBNIsInvalid = "ISBN is invalid";
        public static string PublishYearIsInvalid = "Publish year should be equal or greater than 1800";
        public static string PagesCountIsInvalid = "Pages count should be greater than 0 and less than 10000";

    }
}
