﻿using Bookee.Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookee.Core.BaseClasses
{
    public abstract class Operation
    {
        public int Code { get; private set; }
        public dynamic Result { get; private set; }
        public string Message { get; private set; }

        /// <summary>
        /// Constructs the result that will be sent as a response by the service.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="result"></param>
        /// <param name="message"></param>
        protected virtual void CreateResult(ResponseCodes code, dynamic result, string message)
        {
            this.Code = Convert.ToInt32(code);
            this.Result = result;
            this.Message = message;
        }
    }
}
