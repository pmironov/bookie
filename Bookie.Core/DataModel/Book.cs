﻿using Bookee.Core.Classes;
using Bookee.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bookee.Core.DataModel
{
    public class Book : IBook
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }
        
        [Required(ErrorMessage = "You need to specify at least one author.")]        
        public virtual ICollection<Author> Authors { get; set; }

        [Required(ErrorMessage = "Page count is required.")]
        public int PageCount { get; set; }
        public string Publisher { get; set; }

        [Required(ErrorMessage = "Publication year is required.")]
        public int PublicationYear { get; set; }

        [Required(ErrorMessage = "ISBN is required.")]
        public string ISBN { get; set; }
        public string ImageURL { get; set; }

        /// <summary>
        /// For the sake of meaningful display in runtime peek
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Title;
        }
        
        public void SetPropertiesFrom(IBook otherBook)
        {
            this.ID = otherBook.ID;
            this.Title = otherBook.Title;

            this.Authors = new List<Author>();
            foreach (var author in otherBook.Authors)
            {               
                this.Authors.Add(new Author() { ID = author.ID, FirstName = author.FirstName, LastName = author.LastName });                
            }
            this.PageCount = otherBook.PageCount;
            this.Publisher = otherBook.Publisher;
            this.PublicationYear = otherBook.PublicationYear;
            this.ISBN = otherBook.ISBN;
            this.ImageURL = otherBook.ImageURL;
        }

        public string Validate()
        {
            var res = BookValidationResult.OK;
            if (string.IsNullOrEmpty(this.Title))
            {
                res = string.Format("Book title {0}", BookValidationResult.IsEmpty);
            }
            else if (this.Title.Length > 30)
            {
                res = string.Format("Book title {0}", BookValidationResult.IsTooLong);
            }
            else if (this.Authors == null || this.Authors.Count == 0)
            {
                res = string.Format("Authos list {0}", BookValidationResult.IsEmpty);
            }
            else if (this.PageCount > 10000)
            {
                res = BookValidationResult.PagesCountIsInvalid;
            }
            else if (this.PageCount == 0)
            {
                res = string.Format("Page count {0}", BookValidationResult.IsEmpty);
            }
            else if (this.Publisher.Length > 30)
            {
                res = string.Format("Publisher name {0}", BookValidationResult.IsTooLong);
            }
            else if (this.PublicationYear < 1800)
            {
                res = BookValidationResult.PublishYearIsInvalid;
            }
            else if (this.PublicationYear == 0)
            {
                res = string.Format("Publication year {0}", BookValidationResult.IsEmpty);
            }
            else if (string.IsNullOrEmpty(this.ISBN))
            {
                res = string.Format("ISBN {0}", BookValidationResult.IsEmpty);
            }
            else if (!Regex.Match(this.ISBN, @"(?=.{17}$)97(?:8|9)([ -])\d{1,5}\1\d{1,7}\1\d{1,6}\1\d$").Success)
            {
                res = BookValidationResult.ISBNIsInvalid;
            }
            else
            {
                foreach (var author in this.Authors)
                {
                    if (string.IsNullOrEmpty(author.FirstName))
                    {
                        res = string.Format("One of the authors' first names {0}", BookValidationResult.IsEmpty);
                        break;
                    }
                    else if (author.FirstName.Length > 20)
                    {
                        res = string.Format("One of the authors' first names {0}", BookValidationResult.IsTooLong);
                        break;
                    }
                    else if (string.IsNullOrEmpty(author.LastName))
                    {
                        res = string.Format("One of the authors' last names {0}", BookValidationResult.IsEmpty);
                        break;
                    }
                    else if (author.LastName.Length > 20)
                    {
                        res = string.Format("One of the authors' last names {0}", BookValidationResult.IsTooLong);
                        break;
                    }
                }
            }

            return res;
        }
    }
}
