﻿using Bookee.Core.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;

namespace Bookee.Core.DataModel
{
    public class Author : IAuthor
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "You need to specify at least one author.")]
        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual ICollection<Book> Books { get; set; }

        /// <summary>
        /// For the sake of meaningful display in runtime peek
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.FirstName + this.LastName;
        }
    }
}
