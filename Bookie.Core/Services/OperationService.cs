﻿using Bookee.Core.BaseClasses;
using Bookee.Core.Classes;
using Bookee.Core.Context;
using Bookee.Core.DataModel;
using Bookee.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookee.Core.Services
{
    public class OperationService : Operation, IOperationService
    {
        #region IOperationService implementation

        public void LoadBooks()
        {
            var toReturn = new List<IBook>();

            try
            {
                using (var context = new BooksContext())
                {
                    var books = context.Books.ToList<IBook>();

                    foreach (var b in books)
                    {
                        Book book = new Book();
                        book.SetPropertiesFrom(b);
                        toReturn.Add(book);
                    }
                }
                CreateResult(ResponseCodes.Success, toReturn, ResponseMessages.Success);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Get Books failed: \n" + ex.ToString());
                CreateResult(ResponseCodes.FailedToLoadBooks, null, ResponseMessages.FailedToLoadBooks);
            }
        }

        public void AddBook(IBook bookToAdd)
        {
            var validationResult = bookToAdd.Validate();
            if (validationResult != BookValidationResult.OK)
                CreateResult(ResponseCodes.FailedToAddBook, null, validationResult);
            else
            {
                try
                {
                    using (var context = new BooksContext())
                    {
                        context.Books.Add((Book)bookToAdd);
                        context.SaveChanges();
                    }

                    CreateResult(ResponseCodes.Success, bookToAdd.ID, ResponseMessages.Success);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Add Book failed: \n" + ex.ToString());
                    CreateResult(ResponseCodes.FailedToAddBook, null, ResponseMessages.FailedToAddBook);
                }
            }
        }

        public void EditBook(IBook bookToEdit)
        {
            var validationResult = bookToEdit.Validate();
            if (validationResult != BookValidationResult.OK)
                CreateResult(ResponseCodes.FailedToEditBook, null, validationResult);
            else
            {
                try
                {
                    using (var context = new BooksContext())
                    {
                        var toEdit = context.Books.Where(c => c.ID == bookToEdit.ID).FirstOrDefault();
                        if (toEdit != null)
                        {
                            toEdit.SetPropertiesFrom(bookToEdit);
                            context.SaveChanges();
                        }
                    }

                    CreateResult(ResponseCodes.Success, true, ResponseMessages.Success);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Edit Book failed: \n" + ex.ToString());
                    CreateResult(ResponseCodes.FailedToEditBook, null, ResponseMessages.FailedToEditBook);
                }
            }
        }

        public void RemoveBook(int bookID)
        {
            var failedIds = new List<int>();
            using (var context = new BooksContext())
            {
                var bookToRemove = context.Books.FirstOrDefault(c => c.ID == bookID);
                context.Books.Remove(bookToRemove);

                context.SaveChanges();
            }

            if (failedIds.Count == 0)
                CreateResult(ResponseCodes.Success, true, ResponseMessages.Success);
            else
            {
                Debug.WriteLine(string.Format("Items with IDs {0} were not removed", String.Join(", ", failedIds)));
                CreateResult(ResponseCodes.FailedToRemoveBook, failedIds, ResponseMessages.FailedToRemoveBook);
            }
        }
        #endregion
    }
}
