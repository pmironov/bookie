﻿using Bookee.Core.DataModel;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Bookee.Core.Interfaces
{
    public interface IBook
    {
        int ID { get; set; }
        string Title { get; set; }
        ICollection<Author> Authors { get; set; }
        int PageCount { get; set; }
        string Publisher { get; set; }
        int PublicationYear { get; set; }
        string ISBN { get; set; }
        string ImageURL { get; set; }

        void SetPropertiesFrom(IBook otherBook);
        string Validate();
    }
}
