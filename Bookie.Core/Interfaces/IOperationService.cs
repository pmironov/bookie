﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookee.Core.Interfaces
{
    public interface IOperationService
    {
        /// <summary>
        /// Load list of books.
        /// </summary>
        void LoadBooks();

        /// <summary>
        /// Add new book to the database.
        /// </summary>
        /// <param name="book">Book that we are going to create.</param>
        void AddBook(IBook book);

        /// <summary>
        /// Edit existing book.
        /// </summary>
        /// <param name="book">Exsting book to edit. Its' ID will be used to find the book in the database and other fields are used to update.</param>
        void EditBook(IBook book);

        /// <summary>
        /// Remove book using its' ID.
        /// </summary>
        /// <param name="bookID">ID of the book to remove.</param>
        void RemoveBook(int bookID);
    }
}
