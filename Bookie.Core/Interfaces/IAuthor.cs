﻿using Bookee.Core.DataModel;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Bookee.Core.Interfaces
{
    public interface IAuthor
    {
        int ID { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        [ScriptIgnore(ApplyToOverrides = true)]
        ICollection<Book> Books { get; set; }
    }
}